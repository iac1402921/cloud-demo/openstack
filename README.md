<!-- Improved compatibility of в начало link: See: https://github.com/othneildrew/Best-README-Template/pull/73 -->
<a name="в-начало"></a>
<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Don't forget to give the project a star!
*** Thanks again! Now go create something AMAZING! :D
-->



<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
<!--[![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url]
-->


<!-- openstack-demo-cloud -->
<br />
<div align="center">

# Openstack Demo-cloud
<img src="./images/gitlab_qr.png" alt="gitlab project" width="200"/>
</div>

## Содержание
- [Цель проекта](#цель-проекта)
- [Принцип работы openstack](#принцип-работы-openstack)
- [Архитектура демо-стенда](#архитектура-демо-стенда)
  - [Настройка control node](#настройка-control-node)
  - [Настройка compute node](#настройка-compute-node)
  - [Настройка network node](#настройка-network-node)
  - [Настройка storage node](#настройка-storage-node)
- [Установка openstack](#установка-openstack)
- [Настройка облака](#настройка-облака)
- [PaaS Kubernetes](#paas-kubernetes)
- [Приложение внутри Kubernetes](#приложение-внутри-kubernetes)

[![Openstack][Openstack]][Openstack-url]

<!-- goal -->
# Цель проекта
Исследование принципов работы и возможностей современных облачных решений в домашних условиях

<!-- openstack-arch -->
# Принцип работы openstack
## Верхнеуровневая архитектура
![](images/openstack-map.png)

## Логическая архитектура
![](images/openstack-arch-kilo-logical-v1.png)


<!-- demo-cloud-arc -->
# Архитектура демо стенда
![](images/hwreqs.png)
![](images/network2-services.png)

![](images/demo-cloud.jpeg)

<!-- control-node-settings -->
## Настройка control node
<details>
 <summary><span style="font-size: large; ">Показать код</span></summary>

````shell
# Добавляем пользователя в админы
sudo nano /etc/sudoers
    demoadmin    ALL=(ALL:ALL) ALL

# Меняем hostname
hostnamectl set-hostname d-control-01.ap.com

# устанавливаем curl и git
sudo apt-get update && apt-get install -y curl apt-transport-https git

# Выключаем файл подкачки
sudo swapoff -a
nano /etc/fstab

# Добавляем возможность авторизации под root и с помощью ключей
nano /etc/ssh/sshd_config
    PermitRootLogin yes
    PubkeyAuthentication yes
    PasswordAuthentication yes

systemctl restart sshd

# Устанавливаем Docker
# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/debian/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc

# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/debian \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

docker -v

# Копируем сертификат локального CA (будет нужен в дальнейшем)
nano root.crt
cp root.crt /usr/local/share/ca-certificates/root.crt
update-ca-certificates

## На этом этапе останавливаем VM и копируем ее для создания других нод будущего облака

# Генерим RSA ключ для подключения по ssh
ssh-keygen -t rsa
chmod 644 ~/.ssh/id_rsa.pub 
cat ~/.ssh/id_rsa.pub

# Копируем ключ на остальные хосты
ssh-copy-id -i ~/.ssh/id_rsa.pub root@20.10.0.20
ssh-copy-id -i ~/.ssh/id_rsa.pub root@20.10.0.30
ssh-copy-id -i ~/.ssh/id_rsa.pub root@20.10.0.40
ssh -i ~/.ssh/ostk-rsa root@20.10.0.20

````
</details>

<br><hr>
[🔼 В начало](#openstack-demo-cloud)
<hr>

<!-- Настройка-compute-node -->
## Настройка compute node
<details>
 <summary><span style="font-size: large; ">Показать код</span></summary>

````shell
hostnamectl set-hostname d-compute-01.ap.com
systemctl stop NetworkManager.service
systemctl disable NetworkManager.service
nano /etc/resolv.conf
    search ap.com ap-msk.com apiac.ru
    nameserver 192.168.2.254
    nameserver 192.168.5.254
    options ndots:2 timeout:2 attempts:2

# Настраиваем сеть (для внешней сети без назначения IP адреса)
# Подробнее здесь - https://docs.openstack.org/neutron/latest/install/environment-networking-controller-ubuntu.html  

nano /etc/network/interfaces
    auto lo
    iface lo inet loopback
    
    auto enp6s18
    iface enp6s18 inet manual
            up ip link set dev $IFACE up
            down ip link set dev $IFACE down
    
    auto enp6s19
    iface enp6s19 inet static
            address 20.10.0.20
            netmask 255.255.255.0
            gateway 20.10.0.1
            dns-nameservers 192.168.2.254
            dns-search ap.com ap-msk.com
    
    auto enp6s20
    iface enp6s20 inet manual
            up ip link set dev $IFACE up
            down ip link set dev $IFACE down

/etc/init.d/networking restart
````
</details>

<br><hr>
[🔼 В начало](#openstack-demo-cloud)
<hr>

## Настройка network node
<details>
 <summary><span style="font-size: large; ">Показать код</span></summary>

````shell
hostnamectl set-hostname d-network-01.ap.com

systemctl stop NetworkManager.service
systemctl disable NetworkManager.service
nano /etc/resolv.conf
    search ap.com ap-msk.com apiac.ru
    nameserver 192.168.2.254
    nameserver 192.168.5.254
    options ndots:2 timeout:2 attempts:2

# Настраиваем сеть (для внешней сети без назначения IP адреса)
# Подробнее здесь - https://docs.openstack.org/neutron/latest/install/environment-networking-controller-ubuntu.html  
nano /etc/network/interfaces
    auto lo
    iface lo inet loopback
    
    auto enp6s18
    iface enp6s18 inet manual
            up ip link set dev $IFACE up
            down ip link set dev $IFACE down
    
    auto enp6s19
    iface enp6s19 inet static
            address 20.10.0.30
            netmask 255.255.255.0
            gateway 20.10.0.1
            dns-nameservers 192.168.2.254
            dns-search ap.com ap-msk.com
    
    auto enp6s20
    iface enp6s20 inet manual
            up ip link set dev $IFACE up
            down ip link set dev $IFACE down
/etc/init.d/networking restart
````
</details>

<br><hr>
[🔼 В начало](#openstack-demo-cloud)
<hr>

## Настройка storage node
<details>
 <summary><span style="font-size: large; ">Показать код</span></summary>

````shell
hostnamectl set-hostname d-storage-01.ap.com

# создаем LVM (полезная статья на habr https://habr.com/ru/articles/67283/)
apt-get install lvm2
lsblk
pvcreate /dev/sdb1
vgcreate cinder-volumes /dev/sdb1
vgdisplay

## Размечаем диски для SWIFT
apt-get install xfsprogs

index=0
for d in vda vdb vdc; do
  parted /dev/${d} -s -- mklabel gpt mkpart KOLLA_SWIFT_DATA 1 -1;
  sudo mkfs.xfs -f -L d${index} /dev/${d}1;
  (( index++ ));
done
````
</details>

<br><hr>
[🔼 В начало](#openstack-demo-cloud)
<hr>

<!-- Установка openstack -->

# Установка openstack

<details>
 <summary><span style="font-size: large; ">Показать код</span></summary>

````shell
# Документация здесь - https://docs.openstack.org/kolla-ansible/latest/user/quickstart-development.html

## Устанавливаем python3 и необходимые пакеты
sudo dnf install git python3-devel libffi-devel gcc openssl-devel python3-libselinux
sudo apt install git python3-dev libffi-dev gcc libssl-dev

# install the virtual environment dependencies
sudo apt install python3-venv

# Create a virtual environment and activate it:
mkdir venv
python3 -m venv /home/demoadmin/venv/
source /home/demoadmin/venv/bin/activate

# Устанавливаем pip и ansible
pip install -U pip
pip install 'ansible-core>=2.15,<2.16.99'

# Копируем git-репозиторий kolla-ansible
git clone --branch master https://opendev.org/openstack/kolla-ansible

# Устанавливаем kolla-ansible 
pip install ./kolla-ansible

# Устанавливаем openstackclient и magnumclient
pip install python-openstackclient -c https://releases.openstack.org/constraints/upper/2024.1
pip install python-magnumclient

# создаем отдельную папку в которой будем хранить все настройки (и пароли)
sudo mkdir -p /etc/kolla
sudo chown $USER:$USER /etc/kolla

# Копируем в эту папку содержимое из папки проекта (файлы globals.yml и passwords.yml
cp -r kolla-ansible/etc/kolla/* /etc/kolla
cp kolla-ansible/ansible/inventory/* .

# устанавливаем зависимости ansible galaxy
kolla-ansible install-deps

# Генерируем пароли
cd kolla-ansible/tools
./generate_passwords.py
chmod 600 /etc/kolla/passwords.yml 

# Выполняем настройку kolla-ansible (файл приложен в проекте)
nano /etc/kolla/globals.yml 

# Настраиваем Ring для работы SWIFT
# https://docs.openstack.org/kolla-ansible/latest/reference/storage/swift-guide.html
mkdir -p /etc/kolla/config/swift
STORAGE_NODES=(20.10.0.40)
KOLLA_SWIFT_BASE_IMAGE="kolla/centos-source-swift-base:4.0.0"
   
#Generate Object Ring
    docker run \
      --rm \
      -v /etc/kolla/config/swift/:/etc/kolla/config/swift/ \
      $KOLLA_SWIFT_BASE_IMAGE \
      swift-ring-builder \
        /etc/kolla/config/swift/object.builder create 10 3 1
    
    for node in ${STORAGE_NODES[@]}; do
        for i in {0..2}; do
          docker run \
            --rm \
            -v /etc/kolla/config/swift/:/etc/kolla/config/swift/ \
            $KOLLA_SWIFT_BASE_IMAGE \
            swift-ring-builder \
              /etc/kolla/config/swift/object.builder add r1z1-${node}:6000/d${i} 1;
        done
    done

#Generate Account Ring
    docker run \
      --rm \
      -v /etc/kolla/config/swift/:/etc/kolla/config/swift/ \
      $KOLLA_SWIFT_BASE_IMAGE \
      swift-ring-builder \
        /etc/kolla/config/swift/account.builder create 10 3 1
    
    for node in ${STORAGE_NODES[@]}; do
        for i in {0..2}; do
          docker run \
            --rm \
            -v /etc/kolla/config/swift/:/etc/kolla/config/swift/ \
            $KOLLA_SWIFT_BASE_IMAGE \
            swift-ring-builder \
              /etc/kolla/config/swift/account.builder add r1z1-${node}:6001/d${i} 1;
        done
    done
#Generate Container Ring
    docker run \
      --rm \
      -v /etc/kolla/config/swift/:/etc/kolla/config/swift/ \
      $KOLLA_SWIFT_BASE_IMAGE \
      swift-ring-builder \
        /etc/kolla/config/swift/container.builder create 10 3 1
    
    for node in ${STORAGE_NODES[@]}; do
        for i in {0..2}; do
          docker run \
            --rm \
            -v /etc/kolla/config/swift/:/etc/kolla/config/swift/ \
            $KOLLA_SWIFT_BASE_IMAGE \
            swift-ring-builder \
              /etc/kolla/config/swift/container.builder add r1z1-${node}:6002/d${i} 1;
        done
    done

ls /etc/kolla/config/swift/

# настраиваем хосты на которые будет произведена установка openstack
# Важное замечание в разделе [loadbalancer:children] меняем значение с network на control
# Иначе кластер не будет доступен из внешней сети

nano multinode 

# Запускаем скрипт настройки серверов
kolla-ansible -i multinode bootstrap-servers

# Запускаем скрипт проверки
kolla-ansible -i multinode prechecks

##########
##########
# Долгожданный запуск скриптов установки openstack
kolla-ansible -i multinode deploy  
##########
##########

kolla-ansible -i multinode post-deploy

source /etc/kolla/octavia-openrc.sh 

````
</details>

<br><hr>
[🔼 В начало](#openstack-demo-cloud)
<hr>

<!-- Настройка облака -->
# Настройка облака
#### Добавляем в контейнеры magnum файл конфигурации kubeconfig (Сейчас это **workaround**)
````shell
docker exec -it magnum_api /bin/bash
  mkdir ~/.kube/
  echo "<config file text>" >> ~/.kube/config
docker exec -it magnum_conductor /bin/bash
  mkdir ~/.kube/
  echo "<config file text>" >> ~/.kube/config

docker restart magnum_api
docker restart magnum_conductor
````

<h2>
<details>
  <summary>Environment variable</summary>

![](images/env_var.png)
</details>
</h2>

### Запускаем pipeline из проекта Service:
* В результате будут установлены необходимые Flavor, images и сети

### Запускаем pipeline из проекта vpc-admin/main:
* Будут установлены Security Group, COE Template, сети и виртуальный роутер

### Запускаем pipeline из проекта vpc-admin/k8s-dev-1:
* Устанавливаем PaaS Kubernetes кластер

## IaC, IaC и в продакшн

### [Статья на habr про IaC](https://habr.com/ru/articles/656469/)

<h3>
<details>
  <summary>Типы проектов</summary>

![](images/project_type.png)
</details>
</h3>
<h3>
<details>
  <summary>Схема работы с Security Groups</summary>

![](images/sg.png)
</details>
</h3>



* `i_default/o_default` На виртуальную машину почти всегда назначаются группы по-умолчанию, которые разрешают только самое необходимое – входящие подключения с нод управления, мониторинга и пр., и исходящие во внутреннюю сеть, адреса метаданных Openstack и на репозитории пакетных менеджеров


* `i_int_appname_servicename` Для всех сетевых доступов внутри сети создаются отдельные группы с перечисленными адресами или подсетями, которые должны иметь доступ к конкретному сервису


* `o_int_appname_servicename` Если список облачных хостов неопределён, например, это кластер, или просто слишком велик для ручного управления, то к группам на входящее подключение создаётся комплиметарная группа на исходящее подключение. В этом случае правило входящего подключения привязывается не к конкретному адресу источника, а к имени группы. То есть группа o_svc_appname_servicename здесь служит как некий тег


* `i_ext_appname_servicename` Для доступов извне к сервисам в нашем облачном проекте создаются отдельные группы

<h3>
<details>
  <summary>Схема CI/CD pipeline</summary>

![](images/pipline.png)
</details>
</h3>
<h3>
<details>
  <summary>CI/CD workflow</summary>

![](images/wf.png)
</details>
</h3>


#### Описание:

1. IaC-разработчик создаёт репозиторий в Gitlab из шаблона через Import by URL (ограничение бесплатной версии)
2. Склонировав репозиторий на свою машину, он запускает скрипт для настройки окружения на работу с проектом-песочницей
3. Раскомментировав примеры из шаблона, уже можно создать и конфигурировать первые инстансы
4. Шаблон написан так, чтобы инстансы создавались с локальными парами ssh-ключей и winrm-сертификатов. Если winrm-сертификат ещё не создан, поможет второй скрипт.
5. Чтобы зайти на созданный инстанс Linux, нужно просто набрать ssh user@host, и, так как он был создан с локальной парой ключей, логин пройдёт успешно.
6. Чтобы зайти на Windows-инстанс по RDP, можно использовать четвёртый скрипт для получения и дешифровки уникального пароля от платформы Openstack. Для этого нужно, чтобы при создании инстанса был указан ssh-ключ, точно также, как и для Linux.
7. Примеры Ansible в шаблоне также преднастроены на работу с локальными ключами ssh и WinRM – дополнительные действия не требуются, после создания инстанса можно немедленно запускать его конфигурацию.
8. Те параметры кода, которые должны отличаться между песочницей и боевым проектом, например, флейворы, выносятся в переменные и указываются либо локально, либо в файлах в каталогах environment
9. После того как код готов к деплою в боевое окружение или проверке через CI/CD, разработчик отмечает в .gitlab-ci.yml, какие инструменты и окружения ему нужны и делает push в main или merge request. Пайплайн сам создаст только нужные джобы только в нужных окружениях и заботливо подставит боевые ключи и настроит контейнер на исполнение кода. Изменять код как-то отдельно не нужно.
10. Для траблшутинга (на отладку это не тянет) исполняемого через пайплайн кода предусмотрен вывод в лог переменных окружения и содержимого файлов с переменными в удобном виде.
11. Если разработчик хочет использовать Ansible Vault для хранения части переменных, ему поможет ещё один скрипт – нужно только перенести сгенерированный пароль в CI/CD-переменные Gitlab, и не забыть удалить файл с паролем с локального диска, когда работа закончена.
12. Если разработчику нужно посмотреть список флейворов или образов в проекте, он может использовать вспомогательные джобы пайплайна – просто нажимаешь кнопочку в Gitlab, выполняется запрос через Openstack CLI и выводит информацию.
13. '.gitignore' предотвращает случайную загрузку в репозиторий на Gitlab ключей и локальных стейтов.

### Правила именования сервисов
![](images/service_name.png)
#### Примеры:

* v-sql-qa – Microsoft SQL Server для продакшн-приложения в QA-окружении

* v-iis-app1-1 – Веб-сервер для бизнес-приложения 1, первый из нескольких

* v-gitlab – Сервер Gitlab

### Правила именования Security Groups

![](images/sg_name.png)
#### Примеры:

* i_default – группа по-умолчанию для входящих подключений (мониторинг, управление и т.п.)

* i_int_web_office – доступ к http/https с офисных площадок

* i_ext_smtp – входящие подключения к smtp извне

* o_int_saml – исходящие подключения к сервису saml


<br><hr>
[🔼 В начало](#openstack-demo-cloud)
<hr>

<!-- PaaS Kubernetes -->
# PaaS Kubernetes

### Получаем kubeconfig
````shell
source /home/demoadmin/venv/bin/activate
source /etc/kolla/admin-openrc.sh
export OS_CACERT=/etc/ssl/certs/
export cluster=k8s-dev-1.26.7
openstack coe cluster config $cluster --dir ./
````

### Подключаем кластер к ArgoCD

Добавляем в PaaS кластер Service Account под которым Argo будет подключаться к кластеру

````yaml
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: kubeconfig-sa
secrets:
  - name: kubeconfig-sa-auth-secret
---
apiVersion: v1
kind: Secret
metadata:
  name: kubeconfig-sa-auth-secret
  namespace: kube-system
  annotations:
    kubernetes.io/service-account.name: kubeconfig-sa
type: kubernetes.io/service-account-token
````
Настраиваем RBAC для доступа к ресурсам кластера
```yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: argo-admin
subjects:
  - kind: ServiceAccount
    name: kubeconfig-sa
    namespace: kube-system
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
```
Затем необходимо скопировать токен и CA сертификат из секрета kubeconfig-sa-auth-secret
и привязать PaaS кластер в ArgoCD (это можно сделать добавив secret в namespace, где развернут сам ArgoCD) (ссылка на документацию - https://argo-cd.readthedocs.io/en/stable/operator-manual/declarative-setup/#clusters)

````yaml
apiVersion: v1
kind: Secret
metadata:
  namespace: argocd # same namespace of argocd-app
  name: demo-cloud-k8s-dev-secret
  labels:
    argocd.argoproj.io/secret-type: cluster
type: Opaque
stringData:
  name: k8s-dev-1.26.7 # Get from clusters - name field in config k8s file.
  server: https://192.168.3.198:6443 # Get from clusters - name - cluster - server field in config k8s file.
  config: |
    {
      "bearerToken": "<secret token>",
      "tlsClientConfig": {
        "insecure": false,
        "caData": "<CA Base64>" 
      }
    }
````
## Настраиваем vault интеграцию
#### Настраиваем политику доступа к Intermediate CA (Сам CA настраивается отдельно)
```hcl
path "pki_int_ca*"                      { capabilities = ["read", "list"] }
path "pki_int_ca/sign/ap-com-server"    { capabilities = ["create", "update"] }
path "pki_int_ca/issue/ap-com-server"   { capabilities = ["create"] }
path "pki_int_ca/sign/ap-com-client"    { capabilities = ["create", "update"] }
path "pki_int_ca/issue/ap-com-client"   { capabilities = ["create"] }
```

#### Настраиваем метод аутентификации
[![Метод аутентификации](images/vault-auth.png)](images/vault-auth.png)

#### Настраиваем роль
[![Настройка роли](images/vault-roles.png)](images/vault-roles.png)


<br><hr>
[🔼 В начало](#openstack-demo-cloud)
<hr>

<!-- Приложение внутри Kubernetes -->
# Приложение внутри Kubernetes

Устанавливаем в кластер проект demo-boutique https://github.com/GoogleCloudPlatform/microservices-demo

## Архитектура

[![Архитектура микросервисов](images/architecture-diagram.png)](images/architecture-diagram.png)

## Скриншоты
| Home Page                                                                                                       | Checkout Screen                                                                                                  |
|-----------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------|
| [![Screenshot of store homepage](images/online-boutique-frontend-1.png)](images/online-boutique-frontend-1.png) | [![Screenshot of checkout screen](images/online-boutique-frontend-2.png)](images/online-boutique-frontend-2.png) |


<br><hr>
[🔼 В начало](#openstack-demo-cloud)


<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
<!-- [contributors-shield]: https://img.shields.io/github/contributors/othneildrew/Best-README-Template.svg?style=for-the-badge
[contributors-url]: https://github.com/othneildrew/Best-README-Template/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/othneildrew/Best-README-Template.svg?style=for-the-badge
[forks-url]: https://github.com/othneildrew/Best-README-Template/network/members
[stars-shield]: https://img.shields.io/github/stars/othneildrew/Best-README-Template.svg?style=for-the-badge
[stars-url]: https://github.com/othneildrew/Best-README-Template/stargazers
[issues-shield]: https://img.shields.io/github/issues/othneildrew/Best-README-Template.svg?style=for-the-badge
[issues-url]: https://gitlab.com/iac1402921/cloud-demo/service/-/issues
[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge
[license-url]: https://github.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt -->
[Openstack]: https://img.shields.io/badge/openstack-f20c4b?style=for-the-badge&logo=openstack&logoColor=white
[Openstack-url]: https://www.openstack.org/